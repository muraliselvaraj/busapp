$(document).ready(function () {
	var watchId = null;
	$('#start').click(function() {
			var from = $('#from').val();
			var to = $('#to').val();
			var busno = $('#busno').val();
			var regno = $('#regno').val();
			var options = { timeout: 60000, enableHighAccuracy: true };
			watchId = navigator.geolocation.watchPosition(onSuccess, onError, options);
	    

	    function onSuccess(position) {
			if ( ($('#form').val() == '') || ($('#to').val() == '') || ($('#busno').val() == '') || ($('#regno').val() == '') ){

			} else {
				position.from = $('#from').val();
				position.to = $('#to').val();
				position.busno = $('#busno').val();
				position.regno = $('#regno').val();
				position.location = {
					latitude : position.coords.latitude,
					longitude : position.coords.longitude
				};
			}

			$.ajax({
				url: 'http://54.187.220.194:3000/buses/location',
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				data: JSON.stringify(position)
			})
			.done(function(data) {
				console.log('Success==> ' +data);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			

			alert(JSON.stringify(position));
			alert(watchId);
		}

		function onError(err) {
			alert('code: ' + err.code + '\n' + 'message: ' + err.message + '\n');
		}
	});

	$('#stop').click(function() {
		if (watchId != null) {
			navigator.geolocation.clearWatch(watchId);
			watchId = null;
		}
		alert(watchId);
	});
	
});
